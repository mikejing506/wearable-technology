import { copy, ensureDir, readFile, writeFile } from 'fs-extra';
import { join, resolve } from 'path';
import { Data, Folder } from '../Data';
import { TagSpec, TagsSpec } from '../TagsSpec';
import { chaptersDir, distChaptersDir, distDir, rootDir, staticDir } from './dirs';
import { ErrorReporter } from './ErrorReporter';
import { log } from './indentConsole';
import { LoaderContext } from './LoaderContext';
import { load } from './loaders/Loader';
import { Stats } from './Stats';
import { loadTagsSpec, validateAndBuildTagMap } from './tagsSpecParser';
import yargs = require('yargs');
import { createHash } from 'crypto';
import { readFileSync } from 'fs';
import { stringify } from 'javascript-stringify';

const argv = yargs.options({
  production: { type: 'boolean', default: false },
  suppressError: { type: 'boolean', default: false, alias: 'suppress-error' },
}).argv;

(async () => {
  const errorReporter = new ErrorReporter();
  const startTime = Date.now();

  await ensureDir(distChaptersDir);

  // Load tags spec
  let tagsSpec: null | TagsSpec = null;
  let tagsMap: null | ReadonlyMap<string, TagSpec> = null;
  let tagAliasMap: null | ReadonlyMap<string, string> = null;
  try {
    tagsSpec = await loadTagsSpec();
    ({ tagAliasMap, tagsMap } = await validateAndBuildTagMap(tagsSpec));
  } catch (error) {
    errorReporter.wrapAndReportError('Failed to load tags spec.', error);
  }

  if (tagsSpec !== null) {
    await writeFile(
      resolve(distDir, 'tagsSpec.json'),
      JSON.stringify(tagsSpec, null, argv.production ? 0 : 2),
    );
  }

  const stats = new Stats(argv.production);

  const rootLoaderCtx = new LoaderContext(
    true,
    chaptersDir,
    '',
    stats,
    argv.production,
    errorReporter,
    tagsMap,
  );

  const data: Data = {
    chapterTree: await load(rootLoaderCtx)! as Folder,
    charsCount: argv.production ? stats.getCharsCount() : null,
    paragraphsCount: stats.getParagraphCount(),
    keywordsCount: [...stats.getKeywordsCount()].sort((a, b) => b[1] - a[1]),
    buildNumber: process.env.CI_PIPELINE_IID || 'Unoffical',
    authorsInfo: JSON.parse(await readFile(join(rootDir, 'authors.json'), 'utf8')),
    buildError: errorReporter.hasError(),
    tags: tagsSpec === null ? [] : tagsSpec
      .sort((a, b) => b.priority - a.priority)
      .map(tagSpec => [
        tagSpec.tag,
        tagSpec.variants,
      ]),
    tagAliases: Array.from(tagAliasMap ?? []),
  };
  await writeFile(
    resolve(distDir, 'data.js'),
    `window.DATA=${stringify(data, null, argv.production ? 0 : 2, { skipUndefinedProperties: true })};`,
  );
  log('[[green|data.js created.]]');

  // Copy static
  await copy(staticDir, distDir);
  const indexPath = resolve(distDir, 'index.html');
  let result = await readFile(indexPath, 'utf8');
  const hash = (path: string) => createHash('sha256')
    .update(readFileSync(resolve(distDir, path)))
    .digest('hex')
    .substr(0, 12);
  result = result.replace(/<script src="(.*?)" defer><\/script>/g, (_, path) => {
    return `<script src="${path}?hash=${hash(path)}" defer></script>`;
  });
  result = result.replace(/<link rel="stylesheet" type="text\/css" href="(.*?)">/g, (_, path) => {
    return `<link rel="stylesheet" type="text/css" href="${path}?hash=${hash(path)}">`;
  });
  await writeFile(indexPath, result, 'utf8');

  log('[[green|Static copied.]]');
  log(`[[green|Time spent: [[cyan|${Date.now() - startTime}ms]].]]`);

  if (errorReporter.hasError()) {
    log();
    errorReporter.printAll();
    if (!argv.suppressError) {
      process.exit(1);
    }
  }
})();
